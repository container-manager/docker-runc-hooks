package main

import (
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"golang.org/x/sys/unix"

	"github.com/Jeffail/gabs"
)

var runcCandidates = []string{
	"runc",
	"docker-runc",
	"crun",
	"/usr/bin/runc",  // docker-ce 19.03 path
	"/usr/sbin/runc", // docker.io 19.03 path
}

func findRealRunc() string {
	selfPath, _ := os.Readlink("/proc/self/exe")
	for _, c := range runcCandidates {
		res, err := exec.LookPath(c)
		if err == nil && res != selfPath {
			return res
		}
	}
	panic("unable to find a real runc")
}

// launchRunc replaces this binary with the actual runc binary and executes it
func launchRunc(runcPath string, runcArgs []string) {
	realArgs := append([]string{runcPath}, runcArgs...)

	unix.Exec(runcPath, realArgs, os.Environ())
}

func listHookFiles() []string {
	hookDir := "/etc/docker/hooks.d/"
	files, _ := ioutil.ReadDir(hookDir)
	var res []string
	for _, file := range files {
		res = append(res, hookDir+file.Name())
	}

	return res
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func main() {
	runcArgs := os.Args[1:]

	runcPath := findRealRunc()

	// only manipulate bundle during create or run
	if !contains(os.Args, "create") && !contains(os.Args, "run") {
		launchRunc(runcPath, runcArgs)
	}

	res := gabs.New()
	for _, file := range listHookFiles() {
		hook, err := gabs.ParseJSONFile(file)
		if err != nil {
			panic(err)
		}
		err = res.Merge(hook)
		if err != nil {
			panic(err)
		}
	}

	var bundlePath string
	for i, val := range runcArgs {
		if val == "--bundle" && i != len(runcArgs)-1 {
			// get the bundle Path
			bundlePath = runcArgs[i+1]
			bundlePath = filepath.Join(bundlePath, "config.json")
		}
	}
	bundle, err := gabs.ParseJSONFile(bundlePath)
	if err != nil {
		panic(err)
	}
	res.Merge(bundle)

	err = ioutil.WriteFile(bundlePath, []byte(res.String()), 500)
	if err != nil {
		panic(err)
	}

	launchRunc(runcPath, runcArgs)
}
