This tool allow you to inject arbitrary json into docker's config.json before executing runc. 

## Installation

- `curl -O /usr/bin/docker-runc-hooks https://gitlab.com/api/v4/projects/12257608/jobs/artifacts/master/raw/docker-runc-hooks\?job\=build`
- `chmod +x /usr/bin/docker-runc-hooks`
- Place your "hooks" into `/etc/docker/hooks.d` and configure default runtime to `docker-runc-hooks`
    - See [daemon.json.example](daemon.json.example)
    - See [examples/30-certs.json](examples) for example hooks