module gitlab.com/container-manager/docker-runc-hooks

go 1.12

require (
	github.com/Jeffail/gabs v1.3.1
	golang.org/x/sys v0.0.0-20190509141414-a5b02f93d862
)
